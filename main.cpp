#include <cstdio>
#include <cstdlib>

#include <SDL.h>
#include <glad.h>

static SDL_Window *window = NULL;
static SDL_GLContext glContext;

// Window parameters
static const char title[] = "ImageViewer";
static const int wHeight = 600;
static const int wWidth = 800;
static const int initialPosX = 100;
static const int initialPosY = 100;

static void exitError(const char *mes)
{
    fprintf(stderr, "%s: %s\n", mes, SDL_GetError());
    exit(1);
}

void Init()
{
    // Init SDL2
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        exitError("Could not initialize SDL");
    }

    // Set attributes
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    // Create window
    window = SDL_CreateWindow(title, initialPosX, initialPosY, wWidth, wHeight, SDL_WINDOW_OPENGL);
    if (window == NULL)
    {
        exitError("Could not create window");
    }

    // Create OpenGL context
    glContext = SDL_GL_CreateContext(window);
    if (glContext == NULL)
        exitError("Could not create the OpenGL context");

    // Load OpenGL functions glad SDL
    gladLoadGLLoader(SDL_GL_GetProcAddress);

    // V-Sync
    SDL_GL_SetSwapInterval(1);

    GLint vpWidth, vpHeight;
    SDL_GL_GetDrawableSize(window, &vpWidth, &vpHeight);
    glViewport(0, 0, vpWidth, vpHeight);
}

int main(int argc, char *argv[])
{
    static volatile bool quit = false;
    static SDL_Event event;

    Init();

    while (!quit)
    {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Clear the color buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        SDL_GL_SwapWindow(window);

        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                quit = true;
            }
        }
    }

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return 0;
}
